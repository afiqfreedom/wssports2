<!DOCTYPE html>
<html>
<head>
	<title>WeSports</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/js/jquery-ui/jquery-ui.css">
	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.js"></script>
	<?php include 'config.php'; ?>
	<style type="text/css">
	.kotak{
		margin-top: 150px;
	}

	.kotak .input-group{
		margin-bottom: 20px;
	}
	</style>
</head>
<body>

	<div class="container">
		<?php
		if(isset($_GET['error'])){
			if($_GET['error'] == "404"){
				echo "<div style='margin-bottom:-55px' class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-warning-sign'></span>  Login Error! Please try again!</div>";
			}
		}
		?>
		<div class="panel panel-default">
			<form action="login_act.php" method="post">
				<div class="col-md-4 col-md-offset-4 kotak" style="margin-top: 100px;">
					 <center>
					<h1><a href="#" style="color:#78bbe6;"><span>WeSports</span></a></h1>
	   	         <img src="img/wsl.png" width=130 height=130>
					<h3 style="color:#78bbe6;">Login</h3>
				</center>
			</br>
			</br>
					<div class="form-group">
						<select class="form-control" id="sc" name="sc">
							<option selected="true" disabled="disabled">---- Select Sport Center ----</option>
							<?php
							$sc=mysql_query("select * from sportcenter");
							while($s=mysql_fetch_array($sc)){
								?>
								<option value="<?php echo $s['id']; ?>"><?php echo $s['name'] ?></option>
								<?php
							}
							?>
						</select>
					</div>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="Username" name="uname">
					</div>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
						<input type="password" class="form-control" placeholder="Password" name="pass">
					</div>
					<div class="input-group">
						<input type="submit" style="background-color: #009688; border-color: #009688;" class="btn btn-primary" value="Login">
					</div>
					<div>
						Don't have an account yet? <a href="register.php">Register here</a>
					</div>
					<div>
						Sport center not available? <a href="request.php">Request here</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<footer>
</br>
	<hr style="
    margin-bottom: 20px;
    margin-top: 0px;
	border-top: 1px solid #eee;">
</hr>
<div style="
    margin-left: 100px;
	">
<h5>Copyright &copy; 2017-<?php echo date("Y");?> WeSports </h5>
    <p>Lrt Dang Wangi, Kuala Lumpur<br />
      Email: <a style="color:blue;">afiqfreedom@yahoo.com</a><br />
	  <a href="login_superadmin.php">SA</a>
	  <a href="http://localhost/lobi/">Home</a>
  </div>
</footer>
</html>
