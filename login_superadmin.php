<!DOCTYPE html>
<html>
<head>
	<title>WeSports</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/js/jquery-ui/jquery-ui.css">
	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.js"></script>
	<?php include 'config.php'; ?>
	<style type="text/css">
	.kotak{
		margin-top: 150px;
	}

	.kotak .input-group{
		margin-bottom: 20px;
	}
	</style>
</head>
<body>

	<div class="container">
		<?php
		if(isset($_GET['error'])){
			if($_GET['error'] == "404"){
				echo "<div style='margin-bottom:-55px' class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-warning-sign'></span>  Login Error !! Please try again !!</div>";
			}
		}
		?>
		<div class="panel panel-default">
			<form action="login_superadmin_act.php" method="post">
				<div class="col-md-4 col-md-offset-4 kotak" style="margin-top: 100px;margin-bottom: 150px;">
					 <center>
					<h1><a href="#" style="color:#78bbe6;"><span>WeSports</span></a></h1>
	   	         <img src="img/wsl.png" width=130 height=130>
					<h3 style="color:#78bbe6;">Super Admin Login</h3>
				</center>
			</br>
			</br>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="Username" name="uname">
					</div>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
						<input type="password" class="form-control" placeholder="Password" name="pass">
					</div>
					<div class="input-group">
						<input type="submit" class="btn btn-primary" value="Login">
					</div>
					<div>
						<a href="index.php">Go to Home</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<footer>
</br>
	<hr style="
    margin-bottom: 20px;
    margin-top: 0px;
	border-top: 1px solid #eee;">
</hr>
<div style="
    margin-left: 100px;
	">
<h5>Copyright &copy; 2017-<?php echo date("Y");?> WeSports </h5>
    <p>Lrt Dang Wangi, Kuala Lumpur<br />
      Email: <a style="color:blue;">afiqfreedom@yahoo.com</a><br />
      <a href="login_superadmin.php">SA</a>

  </div>
</footer>
</html>
