-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2018 at 03:24 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wssc`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
`id` int(20) NOT NULL,
  `staff_name` varchar(50) DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_phone` int(20) DEFAULT NULL,
  `staff_ic` int(25) DEFAULT NULL,
  `staff_bday` date DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `staff_name`, `staff_email`, `staff_phone`, `staff_ic`, `staff_bday`, `status`, `sportcenter`) VALUES
(2, ' pedot', 'pedot@y.c', 1235782, 7845123, '2018-05-07', '', 1),
(3, ' pai', 'merican@g.c', 184363, 7845623, '2018-05-07', '', 1),
(4, '   fazy', 'fazy@gmail.com', 3222333, 34567899, '0000-00-00', '', 3),
(5, ' hazy', 'hazy@gm.c', 129117654, 2147483647, '0000-00-00', '', 3),
(9, ' ayoo', 'ayoo@gmail.com', 1365479, 85203478, '2018-05-15', 'active', 4),
(10, ' mariah', 'mariah@gmail.com', 111132223, 2147483647, '0000-00-00', 'inactive', 1),
(11, 'abukl', 'abukl@kl.com', 987654567, 2147483647, '0000-00-00', 'active', 4),
(12, 'test', 'test@test.com', 987654678, 2147483647, '0000-00-00', 'inactive', 4),
(13, 'test1', 'test1@test.com', 1256789876, 2147483647, '2018-10-02', 'active', 4),
(14, 'none', 'none@none.com', 2147483647, 2147483647, '2018-10-09', 'inactive', 4),
(15, 'test3', 'test@test.com', 198765467, 2147483647, '2018-10-02', 'inactive', 4);

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `book_ref` varchar(50) NOT NULL,
  `book_arena` varchar(50) DEFAULT NULL,
  `book_type` varchar(30) DEFAULT NULL,
  `book_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`book_ref`, `book_arena`, `book_type`, `book_date`, `start_time`, `end_time`, `sportcenter`) VALUES
(' 2', ' arena2', 'type2', '2018-09-04', '05:00:00', '06:00:00', 4),
('1', 'arena', 'type', '2018-09-04', '05:00:00', '06:00:00', 3);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `mobile` double NOT NULL,
  `birthday` date NOT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `name`, `email`, `mobile`, `birthday`, `sportcenter`) VALUES
(3, ' fazy', 'afiqfreedom@yahoo.com', 1234567890, '0000-00-00', 3),
(7, 'Ridzuan Salim', 'ridzuan_87@hotmail.com', 172749463, '1995-11-25', 3),
(13, ' Alprince Olleriuuu', 'alal@yahoo.com', 9243453523, '2014-02-11', 1),
(14, ' Alprince Oller lama', 'alal@yahoo.com', 9243453523, '2014-02-11', 4),
(17, '  fazyy', 'afiqfreedom@yahoo.com', 1234567890, '0000-00-00', 7),
(19, '  alis', 'ali@ali.com', 987654321, '1888-09-20', 4),
(20, 'ajk', 'ajk@ajk.ko', 198764567, '4432-03-02', 4);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
`id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` varchar(100) NOT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `full_name`, `email`, `message`, `sportcenter`) VALUES
(1, 'ALbigh Nail Jr', 'nail.jr@nickolodean.co.uk', 'I''m confuse', 4);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
`id` int(30) NOT NULL,
  `events_title` varchar(30) NOT NULL,
  `event_desc` varchar(200) DEFAULT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `events_title`, `event_desc`, `sportcenter`) VALUES
(1, 'event1', 'eventdesc1', 1),
(2, 'event2', 'eventdesc2', 3),
(100, 'delete', 'deletes', 4),
(101, 'event3 ', 'eventdesc3', 4),
(102, ' title3', 'desc3', 4);

-- --------------------------------------------------------

--
-- Table structure for table `financial`
--

CREATE TABLE IF NOT EXISTS `financial` (
`id` int(200) NOT NULL,
  `date` date DEFAULT NULL,
  `book_ref` varchar(50) DEFAULT NULL,
  `item_ref` varchar(50) DEFAULT NULL,
  `cash_in` double(20,0) DEFAULT NULL,
  `cash_out` double(20,0) DEFAULT NULL,
  `total` double(20,0) DEFAULT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financial`
--

INSERT INTO `financial` (`id`, `date`, `book_ref`, `item_ref`, `cash_in`, `cash_out`, `total`, `sportcenter`) VALUES
(1, '2018-10-09', '2', '1', 10, 0, 10, 4),
(2, '2018-10-01', '012121212', '1212', 100, 50, 150, 4),
(3, '2000-04-04', '123', '123', 50, 30, 50, 4);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id` int(30) NOT NULL,
  `news_title` varchar(30) NOT NULL,
  `news_desc` varchar(500) DEFAULT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_title`, `news_desc`, `sportcenter`) VALUES
(1, 'news1', 'newsdesc1', 1),
(2, 'news2', 'newsdesc2', 3),
(100, 'delete', 'deletes', 4),
(101, ' news3', 'newsdesc3', 4),
(102, 'test', 'test', 4);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
`id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `company_name` varchar(30) NOT NULL,
  `ssm_no` varchar(10) NOT NULL,
  `email_address` varchar(30) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(20) NOT NULL,
  `zipcode` int(10) NOT NULL,
  `state` varchar(20) NOT NULL,
  `phone_no` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`id`, `name`, `company_name`, `ssm_no`, `email_address`, `address`, `city`, `zipcode`, `state`, `phone_no`) VALUES
(6, 'Bangsar Sport Center', 'Bangsar Ltd', '6785435678', 'bangsarspc@bangsarltd.nfo', 'bangsar', 'bangsar', 67899, 'bangsar', 126784593),
(7, 'test sc', 'companytest', '1234567890', 'test@test333.com', 'kokdiang', 'kokdiang', 9877, 'Pahang', 187568904);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
`id` int(120) NOT NULL,
  `schedule_date` date DEFAULT NULL,
  `schedule_day` varchar(10) NOT NULL,
  `open_time` double(10,0) DEFAULT NULL,
  `close_time` double(10,0) DEFAULT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `schedule_date`, `schedule_day`, `open_time`, `close_time`, `sportcenter`) VALUES
(1, '2018-08-29', 'monday', 9999999999, 12, 4),
(2, '2018-10-02', 'tuesday', 9999999999, 12, 4),
(3, '2018-10-01', 'monday', 9999999999, 0, 4),
(4, '2018-10-01', 'sunday', 9999999999, 11, 4),
(5, '2018-10-02', 'tuesday', 9999999999, 11, 4),
(6, '2018-10-02', 'sunday', 12, 12, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sportcenter`
--

CREATE TABLE IF NOT EXISTS `sportcenter` (
`id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `company_name` varchar(30) NOT NULL,
  `ssm_no` varchar(10) NOT NULL,
  `email_address` varchar(30) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(20) NOT NULL,
  `zipcode` int(10) NOT NULL,
  `state` varchar(20) NOT NULL,
  `phone_no` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sportcenter`
--

INSERT INTO `sportcenter` (`id`, `name`, `company_name`, `ssm_no`, `email_address`, `address`, `city`, `zipcode`, `state`, `phone_no`) VALUES
(1, 'Sentul Sport Center', 'Std sentul ', '0987654321', 'sentulstd@sc.com', '', 'sentul', 9800, 'wilayah persekutuan', 567894567),
(3, 'Hartamas Sport Center', 'serihartamas co', '1234567890', 'sh@sh.com', 'sh', 'sh', 98777, 'KL', 786543278),
(4, 'Ampang Sport Center', 'Flamingo', '0987654321', 'flaming@nfo.com', 'ampang, KL', 'KL', 65788, 'KL', 156784320),
(6, 'Bangsar Sport Center', 'Bangsar Ltd', '6785435678', 'bangsarspc@bangsarltd.nfo', 'bangsar', 'bangsar', 67899, 'bangsar', 126784593),
(7, 'Ulu Langat Sport Center', 'Langat', '0987654321', 'langat@nfo.com', 'ulu langat, KL', 'KL', 65788, 'KL', 156784320),
(9, 'Putrajaya Sport Center', 'Putra Berhad', '0987654321', 'putra@putra.com', 'putrajaya', 'putrajaya', 9800, 'putrajaya', 986543456),
(10, 'Kinrara Sport Center', 'Putra Berhad', '0987654321', 'putra@putra.com', 'putrajaya', 'putrajaya', 9800, 'putrajaya', 986543456),
(11, 'Johor Sport Center', 'JDT', '1234567890', 'jdt@jdt.com', 'johor', 'johor', 67899, 'johor', 135678435);

-- --------------------------------------------------------

--
-- Table structure for table `superadmin`
--

CREATE TABLE IF NOT EXISTS `superadmin` (
  `id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL,
  `email_address` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `photo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `superadmin`
--

INSERT INTO `superadmin` (`id`, `username`, `email_address`, `password`, `photo`) VALUES
(1, 'superadmin', 'superadmin@wssport.com', 'superadmin', '');

-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

CREATE TABLE IF NOT EXISTS `tips` (
`id` int(30) NOT NULL,
  `tips_title` varchar(30) DEFAULT NULL,
  `tips_desc` varchar(500) DEFAULT NULL,
  `sportcenter` int(11) NOT NULL,
  `image` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tips`
--

INSERT INTO `tips` (`id`, `tips_title`, `tips_desc`, `sportcenter`, `image`) VALUES
(1, 'tips1', 'tipsdesc1', 1, ''),
(2, 'tips2', 'tipsdesc2', 3, ''),
(100, 'delete', 'deletes', 1, ''),
(101, ' tips3', 'tipsdesc3', 4, ''),
(102, ' tips4', 'desc4', 4, ''),
(104, 'gay', 'lgbt', 4, ''),
(105, 'gays2.0', 'lgbtq', 4, '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email_address` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `photo` text NOT NULL,
  `sportcenter` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email_address`, `password`, `photo`, `sportcenter`) VALUES
(1, 'fazrul', 'fazrulafiq@gmail.com', 'fazrul', '', 4),
(2, 'afiq', 'afiq@gmail.com', 'afiq', '', 3),
(3, 'ali', 'ali@ali.com', 'ali123', '', 7),
(4, 'salim', 'salim@gmail.com', 'salim', '', 4),
(5, 'ahseng', 'ahseng@gmai.com', 'ahseng', '', 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
 ADD PRIMARY KEY (`id`), ADD KEY `sportcenter_attendance` (`sportcenter`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
 ADD PRIMARY KEY (`book_ref`), ADD KEY `sportcenter_book` (`sportcenter`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`id`), ADD KEY `sportcenter_client` (`sportcenter`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
 ADD PRIMARY KEY (`id`), ADD KEY `sportcenter_contact` (`sportcenter`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
 ADD PRIMARY KEY (`id`), ADD KEY `sportcenter_events` (`sportcenter`);

--
-- Indexes for table `financial`
--
ALTER TABLE `financial`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_BOOKING` (`book_ref`), ADD KEY `item_ref` (`item_ref`), ADD KEY `sporcenter_financial` (`sportcenter`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`), ADD KEY `sportcenter_news` (`sportcenter`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
 ADD PRIMARY KEY (`id`), ADD KEY `sportcenter_schedule` (`sportcenter`);

--
-- Indexes for table `sportcenter`
--
ALTER TABLE `sportcenter`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `superadmin`
--
ALTER TABLE `superadmin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tips`
--
ALTER TABLE `tips`
 ADD PRIMARY KEY (`id`), ADD KEY `sportcenter_tips` (`sportcenter`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `sportcenter` (`sportcenter`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
MODIFY `id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `financial`
--
ALTER TABLE `financial`
MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
MODIFY `id` int(120) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sportcenter`
--
ALTER TABLE `sportcenter`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tips`
--
ALTER TABLE `tips`
MODIFY `id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
ADD CONSTRAINT `sportcenter_attendance` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
ADD CONSTRAINT `sportcenter_book` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `client`
--
ALTER TABLE `client`
ADD CONSTRAINT `sportcenter_client` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `contact`
--
ALTER TABLE `contact`
ADD CONSTRAINT `sportcenter_contact` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `events`
--
ALTER TABLE `events`
ADD CONSTRAINT `sportcenter_events` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `financial`
--
ALTER TABLE `financial`
ADD CONSTRAINT `sporcenter_financial` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `news`
--
ALTER TABLE `news`
ADD CONSTRAINT `sportcenter_news` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
ADD CONSTRAINT `sportcenter_schedule` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `tips`
--
ALTER TABLE `tips`
ADD CONSTRAINT `sportcenter_tips` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `sportcenter` FOREIGN KEY (`sportcenter`) REFERENCES `sportcenter` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
