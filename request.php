<!DOCTYPE html>
<html>
<head>
	<title>WeSports</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/js/jquery-ui/jquery-ui.css">
	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.js"></script>
	<?php include 'config.php'; ?>
	<style type="text/css">
	.kotak{
		margin-top: 150px;
	}

	.kotak .input-group{
		margin-bottom: 20px;
	}
	</style>
</head>
<body>

	<div class="container">
		<?php
		if(isset($_GET['pesan'])){
			if($_GET['pesan'] == "gagal"){
				echo "<div style='margin-bottom:-55px' class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-warning-sign'></span>  Login Gagal !! Username dan Password Salah !!</div>";
			}
		}
		?>
		<div class="panel panel-default">
			<form action="request_act.php" method="post">
				<div class="col-md-4 col-md-offset-4 kotak" style="margin-top: 100px;">
					 <center>
					<h1><a href="#" style="color:#78bbe6;"><span>WeSports</span></a></h1>
	   	         <img src="img/wsl.png" width=130 height=130>
					<h3 style="color:#78bbe6;">Request Sport Center</h3>
				</center>
            </br>
            </br>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon"></span></span>
						<input type="text" class="form-control" placeholder="Sport Center Name" id="scname" name="scname" required>
					</div>
                    <div class="input-group">
						<span class="input-group-addon"><span class="glyphicon"></span></span>
						<input type="text" class="form-control" placeholder="Company Name" id="comname" name="comname" required>
					</div>
                    <div class="input-group">
						<span class="input-group-addon"><span class="glyphicon"></span></span>
						<input type="text" min="10" max="10" class="form-control" placeholder="SSM Reference Number" id="ssm" name="ssm" required>
					</div>
					<div class="input-group">
						<span class="input-group-addon"></span>
						<input type="date" class="form-control" placeholder="Date" name="date">
					</div> 
                    <div class="input-group">
						<span class="input-group-addon"><span class="glyphicon"></span></span>
						<input type="email" class="form-control" placeholder="Email Address" id="email" name="email" required>
					</div>
                    <div class="input-group">
						<span class="input-group-addon"><span class="glyphicon"></span></span>
						<input type="text" class="form-control" placeholder="Address" id="address" name="address" required>
					</div>
                    <div class="input-group">
						<span class="input-group-addon"><span class="glyphicon"></span></span>
						<input type="text" class="form-control" placeholder="City" id="city" name="city" required>
					</div>
                    <div class="input-group">
						<span class="input-group-addon"><span class="glyphicon"></span></span>
						<input type="number" class="form-control" placeholder="Zip Code" id="zipcode" name="zipcode" required>
					</div>

					<div>
					<select class="form-control" name="state">
					<option selected="true" disabled="disabled" value=""> ---State--- </option>
						<option value="Perlis">Perlis</option>
						<option value="Kedah">Kedah</option>
						<option value="Penang">Penang</option>
						<option value="Perak">Perak</option>
						<option value="Pahang">Pahang</option>
						<option value="Selangor">Selangor</option>
						<option value="Wilayah Persekutuan">Wilayah Persekutuan</option>
						<option value="Melaka">Melaka</option>
						<option value="Negeri Sembilan">Negeri Sembilan</option>
						<option value="Johor">Johor</option>
						<option value="Terenganu">Terenganu</option>
						<option value="Kelantan">Kelantan</option>
						<option value="Sabah">Sabah</option>
						<option value="Sarawak">Sarawak</option>
					</select>
				</div>
			</br>

                    <div class="input-group">
						<span class="input-group-addon"><span class="glyphicon"></span></span>
						<input type="tel" pattern="[0-9]{10}" class="form-control" placeholder="Phone Number" id="tel" name="tel" required>
					</div>

				<label>Upload an Image :</label>
					<div class="input-group">
						<span class="input-group-addon"></span>
					<input class="form-control"  type="file" accept=".jpg,.png,.gif,.jpeg" name="image">
				</div>

					<div class="input-group">
						<input type="submit" style="background-color: #009688; border-color: #009688;" class="btn btn-primary" value="Submit Request">
					</div>
					<div>
						<a href="index.php">return home</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<footer>
</br>
	<hr style="
    margin-bottom: 20px;
    margin-top: 0px;
	border-top: 1px solid #eee;">
</hr>
<div style="
    margin-left: 100px;
	">
<h5>Copyright &copy; 2017-<?php echo date("Y");?> WeSports </h5>
    <p>Lrt Dang Wangi, Kuala Lumpur<br />
      Email: <a style="color:blue;">afiqfreedom@yahoo.com</a><br />
      <a href="login_superadmin.php">SA</a>
  </div>
</footer>
</html>
